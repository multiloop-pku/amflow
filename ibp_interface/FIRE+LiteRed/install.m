(* ::Package:: *)

(*FIRE installation directory*)

(*For Unix:*)
$FIREInstallationDirectory = "/afs/cern.ch/user/x/xiaol/research/mmapkg/external/fire/FIRE6";

(*For Windows with WSL:*)
(*$FIREInstallationDirectory = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fire\\FIRE6";*)


(*FIRE version*)
$FIREVersion = 6;
