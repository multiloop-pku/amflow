(* ::Package:: *)

(*For Unix:*)


(*path to FiniteFlow.m*)
$FFPath = "/afs/cern.ch/user/x/xiaol/research/mmapkg/external/finiteflow";
(*path to fflowmlink.so (in Linux) or fflowmlink.dylib (in MacOS)*)
$FFLibraryPath = "/afs/cern.ch/user/x/xiaol/research/local/ffmath";
(*path to LiteIBP.m*)
$LiteIBPPath = "/afs/cern.ch/user/x/xiaol/research/mmapkg/external/finiteflow-mathtools-master/packages";
(*path to LiteRed.m*)
$LiteRedPath = "/afs/cern.ch/user/x/xiaol/research/mmapkg/external/LiteRed/Setup";


(*For Windows with WSL:*)



(*(*path to FiniteFlow.m*)
$FFPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fflow\\finiteflow-master\\mathlink";
(*path to fflowmlink.so (in Linux) or fflowmlink.dylib (in MacOS)*)
$FFLibraryPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fflow\\finiteflow-master";
(*path to LiteIBP.m*)
$LiteIBPPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fflow\\finiteflow-mathtools-master\\packages";
(*path to LiteRed.m*)
$LiteRedPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\LiteRed\\Setup";
*)
