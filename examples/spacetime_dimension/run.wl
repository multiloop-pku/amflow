(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "Blade"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = bubble;
AMFlowInfo["Loop"] = {l1, l2};
AMFlowInfo["Leg"] = {};
AMFlowInfo["Conservation"] = {};
AMFlowInfo["Replacement"] = {};
AMFlowInfo["Propagator"] = {l1^2-1, l2^2-1, (l1+l2)^2-1};
AMFlowInfo["Numeric"] = {};
AMFlowInfo["NThread"] = 4;


(*"D0" \[Rule] 4, by default*)
(*by changing this option to D0, where D0 could be any rational number in principle, SolveIntegrals will compute integrals with D = D0-2eps and give expansions near eps=0*)
SetAMFOptions["D0" -> 7/3];


(*SolveIntegrals: computes given integrals with given precision goal up to given eps order*)
(*returned is a list of replacement rules like {j1 \[Rule] v1, j2 \[Rule] v2, ...}, where j1, j2, ... are integrals and v1, v2, ... are their results*)
integrals = {j[bubble,1,1,0], j[bubble,1,1,1]};
precision = 20;
epsorder = 5;
sol73D = SolveIntegrals[integrals, precision, epsorder];
Put[sol73D, FileNameJoin[{current, "sol73D"}]];


(*also compute with D=1/3-2eps*)
SetAMFOptions["D0" -> 1/3];
sol13D = SolveIntegrals[integrals, precision, epsorder];
Put[sol13D, FileNameJoin[{current, "sol13D"}]];


(*check the results with difference equations of master integrals which can be derived from dimensional recurrence relations*)
(*I[d-2] \[Equal] A[d] . I[d], where I[d] is a list of master integrals (in d-dimension) and A[d] is a matrix*)
(*here the two master integrals are j[bubble,1,1,0] and j[bubble,1,1,1]*)
A = {{1/4*(-2 + d)^2, 0}, {-1/4*(-2 + d)^2, 1/3*(-3 + d)*(-2 + d)}};
(integrals/.sol13D)-(A/.d->7/3-2eps) . (integrals/.sol73D)+O[eps]^2


Quit[];
