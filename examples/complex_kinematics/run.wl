(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "FIRE+LiteRed"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = box;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> p3sq, p4^2 -> p4sq, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p4)^2-m3sq};
AMFlowInfo["Numeric"] = {s -> 496, t -> -39, p3sq -> 7/2, p4sq -> 8, m3sq -> 2-I};
AMFlowInfo["NThread"] = 4;


(*SolveIntegrals: computes given integrals with given precision goal up to given eps order*)
(*returned is a list of replacement rules like {j1 \[Rule] v1, j2 \[Rule] v2, ...}, where j1, j2, ... are integrals and v1, v2, ... are their results*)
(*the results have been validated with eq.(4.29) of arXiv: 1005.2076*)
integrals = {j[box,1,1,1,1]};
precision = 40;
epsorder = 2;
sol = SolveIntegrals[integrals, precision, epsorder];
Put[sol, FileNameJoin[{current, "sol"}]];


Quit[];
