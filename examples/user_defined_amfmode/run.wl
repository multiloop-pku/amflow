(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "FiniteFlow+LiteRed"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = box1;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p4^2 -> 0, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p4)^2};
AMFlowInfo["Numeric"] = {s -> 100, t -> -1};
AMFlowInfo["NThread"] = 4;


(*SetAMFOptions["AMFMode" \[Rule] {keyword1, keyword2,...}]*)
(*built-in keywords are "Prescription", "Mass", "Propagator", "Branch", "Loop" and "All"*)
(*each keyword corresponds to an preference to insert eta*)
(*"Prescription": to insert eta to all propagators with -1 prescription*)
(*"Mass": to insert eta to all propagators with equal non-zero mass*)
(*"Propagator": to insert eta to a single propagator*)
(*"Branch": to insert eta to propagators that form a branch of the topology*)
(*"Loop": to insert eta to propagators that form a loop of the topology*)
(*"All": to insert eta to all propagators*)
(*in all cases, eta will never be inserted to cut propagators*)
(*the program will adopt the first keyword in "AMFMode" that can generate a nontrivial insertion*)
(*default value of this option is {"Prescription", "Mass", "Propagator"}*)
(*here we try to define a new keyword "usr", to insert eta in an mandatory way at the first iteration*)
SetAMFOptions["AMFMode" -> {"usr"}];


(*we need to define the function AMFPosition[top, "usr"], where top is a list of integer to sepcify the top sector*)
(*the function will tell the program to insert eta to the first and the third propagator if the top sector have all the four propagators*)
(*in other cases, we use the default mode to insert eta*)
AMFPosition[top_, "usr"]:=If[top==={1,2,3,4}, {1,3}, AMFPosition[top, "AMFMode"/.Options[SetAMFOptions]]];


(*SolveIntegrals: computes given integrals with given precision goal up to given eps order*)
(*returned is a list of replacement rules like {j1 \[Rule] v1, j2 \[Rule] v2, ...}, where j1, j2, ... are integrals and v1, v2, ... are their results*)
integrals = {j[box1,2,0,1,0], j[box1,-2,1,1,2], j[box1,1,2,2,1]};
precision = 50;
epsorder = 5;
sol = SolveIntegrals[integrals, precision, epsorder];
Put[sol, FileNameJoin[{current, "sol"}]];


Quit[];
