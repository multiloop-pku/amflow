(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "Kira"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = gauge;
AMFlowInfo["Loop"] = {l1, l2, l3};
AMFlowInfo["Leg"] = {n};
AMFlowInfo["Conservation"] = {};
AMFlowInfo["Replacement"] = {n^2 -> -1};
AMFlowInfo["Propagator"] = {l1^2, l2^2, l3^2, 1 + l1*n, 1/2 + l1*n + l2*n + l3*n, l1^2 + 2*l1*l2 + l2^2, l1^2 + 2*l1*l3 + l3^2, l2^2 + 2*l2*l3 + l3^2, -1 + l2^2 + 2*l2*n};
AMFlowInfo["Numeric"] = {};
AMFlowInfo["NThread"] = 4;


(*SolveIntegralsGaugeLink: computes given integrals with given precision goal up to given eps order*)
(*returned is a list of replacement rules like {j1 \[Rule] v1, j2 \[Rule] v2, ...}, where j1, j2, ... are integrals and v1, v2, ... are their results*)
integrals = {j[gauge, 1,1,1,1,1,-1,0,0,0], j[gauge, 1,1,1,1,1,0,-1,0,0], j[gauge, 1,1,1,1,1,0,0,-1,0], j[gauge, 1,1,1,1,1,0,0,0,-1]};
precision = 20;
epsorder = 10;
sol = SolveIntegralsGaugeLink[integrals, precision, epsorder];
Put[sol, FileNameJoin[{current, "sol"}]];


Quit[];
