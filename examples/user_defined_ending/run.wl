(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "FIRE+LiteRed"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = box1;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p4^2 -> 0, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p4)^2};
AMFlowInfo["Numeric"] = {s -> 100, t -> -1};
AMFlowInfo["NThread"] = 4;


(*reduce target integrals to preferred master integrals*)
target = {j[box1,2,0,1,0], j[box1,-2,1,1,2], j[box1,1,2,2,1]};
preferred = {};
{masters, rules} = BlackBoxReduce[target, preferred];


(*automatically define the list of eps-values, truncated order and working precision for following numerical evaluations*)
precision = 50;
epsorder = 5;
{epslist, xorder, wkpre} = GenerateNumericalConfig[precision, epsorder];
SetAMFOptions["XOrder" -> xorder, "WorkingPre" -> wkpre];


(*SetAMFOptions["EndingScheme" \[Rule] {keyword1, keyword2,...}], default (built-in) keywords: {"Tradition", "Cutkosky", "SingleMass", "Trivial"}*)
(*each keyword corresponds to an ending scheme, which will define a type of ending boundary integrals*)
(*"Tradition": the ending boundary integrals are either single-mass vacuum integrals or pure phase-space volumes*)
(*"Cutkosky": pure phase-space volumes will be transformed to loop integrals and evaluated using Cutkosky rules*)
(*"SingleMass": single-mass vacuum integrals will be transformed to p-integrals and evaluated*)
(*"Trivial": defined to deal with other type of integrals*)
(*so by default, we won't need to input any boundary integrals. The ending systems are trivial (0-loop).*)
(*here we first try the traditional scheme which will end up with single-mass vacuum integrals*)
SetAMFOptions["EndingScheme" -> {"Tradition"}];


(*choose a working directory*)
$AMFDirectory = CacheDirectory["test1"]


(*set up all differential systems*)
sysids = AMFSystemsSetup[masters]


(*evaluate the systems to obtain numerical results. however, this cannot be successful due to lack of boundary integrals*)
sol = AMFSystemsSolution[sysids, epslist];


(*check the ending systems*)
ending = Join@@(AMFSystemsEnding/@sysids)


(*read the information of integrals for system 3, using AMFSystemRead[systemID, keyword]*)
(*it can be straightforwardly seen that this integral is an one-loop tadpole, whose result is -Gamma[-1+eps]*)
AMFSystemRead[3, "Config"]
AMFSystemRead[3, "Preferred"][[3]]


(*write the results of integrals to system 3, using AMFSystemWrite[systemID, keyword, expression]*)
AMFSystemWrite[3, "Solution", {j[box1,1,0,0,0] -> -Gamma[-1+epslist]}]


(*also for system 4*)
AMFSystemRead[4, "Config"]
AMFSystemRead[4, "Preferred"][[3]]
AMFSystemWrite[4, "Solution", {j[box1,1,0,0,0] -> -Gamma[-1+epslist]}]


(*next we can evaluate the systems to obtain numerical results*)
sol = AMFSystemsSolution[sysids, epslist];


(*fit the expansion of master integrals and obtain final results for target integrals*)
leading = -2;
values = Transpose@MapThread[(Values[rules]/.eps->#1) . #2&, {epslist, Transpose[masters/.sol]}];
exp = FitEps[epslist, #, leading]&/@values;
final = Thread[Keys[rules] -> Chop@N[Normal@Series[exp, {eps, 0, leading+epsorder}], precision]];


(*save the results*)
Put[final, FileNameJoin[{current, "final_Tradition"}]];


(*next we try to define a new ending scheme named "usr", which has an additional ending at one-loop massless bubble integrals*)
(*built-in schemes: "Tradition", "Cutkosky", "SingleMass", "Trivial"*)
SetAMFOptions["EndingScheme" -> {"user"}];


(*two functions should be defined for this scheme: AMFSystemEndingQ[masters_, "user"] and AMFSystemSetupMaster[masters_, "user"]*)
(*AMFSystemEndingQ[masters, scheme] checks whether the system is an ending system with respect to the chosen scheme. True means an ending and False means not an ending.*)
(*for our purpose, it suffices to define a function which treated integrals with two propagators as ending*)
AMFSystemEndingQ[masters_, "user"]:=Length[GetTopPosition[masters]]===2;


(*AMFSystemSetupMaster[masters, scheme] sets up system with the chosen ending scheme, should only work when AMFSystemEndingQ[masters, scheme] yields False*)
(*because we aim to introduce an additional ending, so no further coding is needed, we just use the default schemes to deal with integrals that are not treated as ending by "user"*)
AMFSystemSetupMaster[masters_, "user"]:=AMFSystemSetupMaster[masters, {"Tradition", "Cutkosky", "SingleMass", "Tivial"}];


(*choose a working directory*)
$AMFDirectory = CacheDirectory["test2"]


(*set up all differential systems*)
sysids = AMFSystemsSetup[masters]


(*check the ending systems*)
ending = Join@@(AMFSystemsEnding/@sysids)


(*read the information of integrals for system 2*)
(*it can be straightforwardly seen that this integral is a one-loop massless bubble, whose result is Gamma[1-eps]^2Gamma[eps]/Gamma[2-2eps]*)
AMFSystemRead[2, "Config"]
AMFSystemRead[2, "Preferred"][[3]]


(*write the results of integrals to system 2*)
AMFSystemWrite[2, "Solution", {j[box1,1,0,1,0] -> Gamma[1-eps]^2Gamma[eps]/Gamma[2-2eps]/.eps->epslist}]


(*read the information of integrals for system 3*)
(*this is the so-called 0-loop integral, whose result can be generated automatically by AMFlow, so we don't need to input anything*)
AMFSystemRead[3, "Config"]
AMFSystemRead[3, "Preferred"][[3]]


(*next we can evaluate the systems to obtain numerical results*)
sol = AMFSystemsSolution[sysids, epslist];


(*fit the expansion of target integrals*)
leading = -2;
values = Transpose@MapThread[(Values[rules]/.eps->#1) . #2&, {epslist, Transpose[masters/.sol]}];
exp = FitEps[epslist, #, leading]&/@values;
final2 = Thread[Keys[rules] -> Chop@N[Normal@Series[exp, {eps, 0, leading+epsorder}], precision]];


(*save the results*)
Put[final2, FileNameJoin[{current, "final_usr"}]];


Quit[];
