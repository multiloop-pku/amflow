(* ::Package:: *)

(* ::Subsubsection::Closed:: *)
(*prepare*)


(*ensure that you have executed run.wl before running this one*)


(*load the package DESolver.m, which is designed for solving differential equations numerically*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "diffeq_solver/DESolver.m"}]];


(*get from file the differential equation with respect to s*)
(*NOTE: the differential variable must be replaced by eta*)
{master, diffeq} = Get[FileNameJoin[{current, "diffeq"}]]/.{s -> eta, eps -> 10^-4}//Together;


(*get the "amplitude"*)
amp = j[sunrise,1,1,1,-3,0]/.Get[FileNameJoin[{current, "redtable"}]]/.eps->10^-4;
amp = Collect[amp, _j, Together]
ampcoe = Normal[CoefficientArrays[amp, master][[2]]]


(*values at the starting point s = 1/2*)
boundary = Get[FileNameJoin[{current, "sol1"}]];
boundary = (master/.boundary)[[All,1]];


(*values at the checking point s = 1/10*)
check = Get[FileNameJoin[{current, "sol2"}]];
check = (master/.check)[[All,1]];


(*necessary, set the default options for numerical solving*)
SetDefaultOptions[];


(* ::Subsubsection::Closed:: *)
(*check numerical inputs*)


(*define a system, where 1/2 means the boundary point s = 1/2*)
LoadSystem["check", diffeq, boundary, 1/2];


(*get the list of poles of this differential equation*)
polelist = GetPoles[DE["check"]]


(*define a path for continuation from s=1/2 to s=1/10*)
run = RunSegment[polelist, 1/2, 1/10]


(*run on regular points*)
RegularRun["check", run];


(*relative error*)
(BC["check"]-check)/(BC["check"]+check)//Abs//Max


(*clear the system and release some RAM*)
ClearSystem["check"];


(* ::Subsubsection::Closed:: *)
(*asymptotic expansion near s=0*)


(*define a system, where 1/2 means the boundary point s = 1/2*)
LoadSystem["asyexp0", diffeq, boundary, 1/2];


(*compute the expansions of master integrals near s=0*)
SolveAsyExp["asyexp0"];


(*calculate expansion of "amplitude" near s=0*)
(*it can be seen this is a Taylor expansion, so s=0 is in fact a pseudo singularity*)
asyexp0 = PlusAsyExp@MapThread[TimesAsyExp, {ampcoe/.{s -> eta}, AsyExp["asyexp0"]}];
asyexp0 = Chop[Expand@Total[EvaluateAsymptoticExpansion[#, s]&/@asyexp0]]


Put[asyexp0, FileNameJoin[{current, "asyexp0"}]];


(*clear the system and release some RAM*)
ClearSystem["asyexp0"];


(* ::Subsubsection::Closed:: *)
(*asymptotic expansion near s=1*)


(*the program can only solve the expansion at 0, so a transformation is needed*)
LoadSystem["asyexp1", -diffeq/.{eta -> 1-eta}, boundary, 1/2];


(*compute the expansions of master integrals near 1-s=0*)
SolveAsyExp["asyexp1"];


(*calculate expansion of "amplitude" near 1-s=0*)
(*the expansion consists of two parts, a[n](1-s)^n and (1-s)^(3-4eps) b[n](1-s)^n*)
asyexp1 = PlusAsyExp@MapThread[TimesAsyExp, {ampcoe/.{s -> 1-eta}, AsyExp["asyexp1"]}];
asyexp1 = Chop[Expand@Total[EvaluateAsymptoticExpansion[#, s]&/@asyexp1]]/.{s -> 1-s}


Put[asyexp1, FileNameJoin[{current, "asyexp1"}]];


(*clear the system and release some RAM*)
ClearSystem["asyexp1"];


(* ::Subsubsection::Closed:: *)
(*asymptotic expansion near s=1 - fit*)


(*the program can only solve the expansion at 0, so a transformation is needed*)
LoadSystem["asyexp1", -diffeq/.{eta -> 1-eta}, boundary, 1/2];


(*imagine there is a pseudo singularty at s = 1-I/50, which will affect the numerical stability of the expansion*)
(*we first define a path for continuation from s=1/2 to s=99/100, where we will divide the boundary conditions*)
polelist = GetPoles[DE["asyexp1"]];
run = RunSegment[polelist, 1/2, 1/100]


(*obtain the asymptotic expansions near s=1*)
RegularRun["asyexp1", run];
SolveAsyExp["asyexp1"];


(*divide the boundary conditions at s=99/100 into two parts*)
bc1 = EvaluateAsymptoticExpansion[#, 1/100]&/@AsyExp["asyexp1"][[All, 1]];
bc2 = EvaluateAsymptoticExpansion[#, 1/100]&/@AsyExp["asyexp1"][[All, 2]];


(*compute the first part of master integrals and the "amplitude" around a circle with r=1/2 centered at s=1*)
BC["asyexp1"] = bc1;
P["asyexp1"] = 1/100;
RegularRun["asyexp1", RunSegment[polelist, 1/100, 1/2]];
samples = Rationalize[N[Power[E, 2Pi*I {Range[0,50]/101, Reverse@Range[51, 100]/101}]/2], 10^-6];
interpolations = RegularInterpolation["asyexp1", #]&/@samples;
ampinter = Total/@(Transpose[ampcoe/. s->1-Join@@samples] Join@@interpolations);


(*fit the expansion of the first part*)
part1 = (Chop@Fit[Transpose@{Join@@samples, ampinter}, s^Range[0,100], s]/.s->1-s);


(*the second part*)
BC["asyexp1"] = bc2;
P["asyexp1"] = 1/100;
RegularRun["asyexp1", RunSegment[polelist, 1/100, 1/2]];
samples = Rationalize[N[Power[E, 2Pi*I {Range[0,50]/101, Reverse@Range[51, 100]/101}]/2], 10^-6];
interpolations = RegularInterpolation["asyexp1", #]&/@samples;
ampinter = Total/@(Transpose[ampcoe/. s->1-Join@@samples] Join@@interpolations);


(*fit the expansion of the second part*)
part2 = (Chop@Fit[Transpose@{Join@@samples, ampinter}, s^(7499/2500) s^Range[0,100], s]/.s->1-s);


Put[part1+part2, FileNameJoin[{current, "asyexp1-fit"}]];


(*clear the system and release some RAM*)
ClearSystem["asyexp1"];


Quit[];
